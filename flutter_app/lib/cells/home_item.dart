import 'package:flutter/material.dart';
import 'package:flutter_app/models/posts.dart';
import 'package:flutter_app/screens/post_detail.dart';
import 'package:flutter_app/models/categories.dart';
import 'package:flutter_app/custom_widgets/pop_menu.dart';
import 'package:flutter_app/utlis/html.dart';
import 'package:cached_network_image/cached_network_image.dart';


class HomeItem extends StatefulWidget{

  final Post _post;
  final Category category;

  HomeItem(this._post, this.category);

  @override
  HomeItemState createState() => HomeItemState(_post,category);

}


class HomeItemState extends State<HomeItem>{

  Post _post;
  Category category;

  HomeItemState(this._post,this.category);

  @override
  Widget build(BuildContext context) {

    var isAd = category.name.toLowerCase() == "ads";

    return ListTile(
      contentPadding: EdgeInsets.fromLTRB(10, 10, 0, 10),

      leading:
      CachedNetworkImage(
        width: 80,
        height: 60,
        fit: BoxFit.cover,
        imageUrl: _post.imageUrl,
        placeholder: (context, url) => new CircularProgressIndicator(),
        errorWidget: (context, url, error) => new Icon(Icons.error),
      ),

      title: Text(
      _post.title,
      style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
    ),

      trailing: isAd ? null : PopUpMenu(_post,Colors.grey),

      onTap: (){

        String url = _post.link;

        if (isAd)
          url = Html.removeTags(_post.content).trim();

        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => PostDetailScreen(_post,url,isAd)),
        );
      },

    );
  }

}
