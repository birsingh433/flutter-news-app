

class Constants{

  static const BASE_URL = #BASE_URL;

  static const COMMENTS = 'Comments';
  static const APP_NAME = 'TOS';
  static const ADD_COMMENT = 'Add Comment';
  static const COMMENT_HINT = 'Enter your comment';
  static const COMMENT = 'Comment';
  static const COMMENT_ERROR = 'Please enter comment';
  static const LOGIN = 'Login';
  static const WELCOME = 'Welcome to TOS';
  static const USERNAME_HINT = 'Enter your username';
  static const USERNAME = 'Username';
  static const USERNAME_ERROR = 'Please enter username';
  static const PASSWORD_HINT = 'Enter your password';
  static const PASSWORD = 'Password';
  static const PASSWORD_ERROR = 'Please enter password';
  static const REGISTER = 'Register?';
  static const SIGN_UP = '  Sign up';
  static const NAME_HINT = 'Enter your name';
  static const NAME = 'Name';
  static const NAME_ERROR = 'Please enter name';
  static const EMAIL_HINT = 'Enter your email';
  static const EMAIL = 'Email';
  static const EMAIL_ERROR = 'Please enter email';
  static const CONFIRM_PASSWORD = 'Confirm password';
  static const PASSWORD_DID_NOT_MATCH = 'Password does not match';
  static const LOADING = 'Loading...';
  static const COMMENT_SUCCESS = 'Commented successfully';
  static const LOGIN_SUCCESS = 'Login successfully';
  static const SIGN_UP_SUCCESS = 'SignUp successfully';
  static const REMOVED = 'Removed to saved posts';
  static const ADDED = 'Added to saved posts';
  static const SAVED_POSTS = 'Saved posts';
  static const LOGOUT = 'Logout';
  static const LOGOUT_MESSAGE = 'Are you sure to logout';
  static const YES = 'Yes';
  static const NO = 'No';
  static const REMOVE = 'Remove';
  static const SAVE = 'Save';
  static const SHARE = 'Share';
  static const MORE = 'MORE';
  static const NO_POSTS = 'No posts';

}