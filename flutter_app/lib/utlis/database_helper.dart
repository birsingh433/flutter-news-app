import 'dart:async';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:flutter_app/models/posts.dart';

class DatabaseHelper {

  static DatabaseHelper _databaseHelper;
  static Database _database;

  String postTable = 'post_table';
  String collId = 'id';
  String collTitle = 'title';
  String collImageUrl = 'imageUrl';
  String collLink = 'link';
  String collContent = 'content';

  DatabaseHelper._instance();


  factory DatabaseHelper() {

    if (_databaseHelper == null) {
      _databaseHelper = DatabaseHelper._instance();
    }
    return _databaseHelper;
  }


  Future<Database> get database async {

    if (_database == null) {
      _database = await initializeDatabase();
    }
    return _database;
  }


  Future<Database> initializeDatabase() async {
    // Get the directory path for both Android and iOS to store database.
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + 'posts.db';

    // Open/create the database at a given path
    var postsDatabase = await openDatabase(path, version: 1, onCreate: _createDb);
    return postsDatabase;
  }


  void _createDb(Database db, int newVersion) async {
    await db.execute('CREATE TABLE $postTable($collId INTEGER PRIMARY KEY AUTOINCREMENT, $collTitle TEXT, '
        '$collImageUrl TEXT, $collLink TEXT, $collContent TEXT)');
  }

  Future<int> insertPost(Post post) async {
    Database db = await this.database;
    var result = await db.insert(postTable, post.toMap());
    return result;
  }

  Future<int> deletePost(Post post) async {
    var db = await this.database;
    var id = post.id;
    int result = await db.rawDelete('DELETE FROM $postTable WHERE $collId = $id');
    return result;
  }

  Future<List<Map<String, dynamic>>> getPostMapList() async {
    Database db = await this.database;
    var result = await db.query(postTable);
    return result;
  }
  
  Future<bool> isPostSaved(Post post) async {
    Database db = await this.database;
    List<Map> results = await db.query(postTable,
        columns: [collId],
        where: 'id = ?',
        whereArgs: [post.id]);

    if (results.length > 0) {
      return true;
    }

    return false;
  }

}