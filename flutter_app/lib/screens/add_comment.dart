import 'package:flutter/material.dart';
import 'package:flutter_app/utlis/constants.dart';
import 'package:flutter_app/models/posts.dart';
import 'package:flutter_app/network/comment_service.dart';
import 'dart:async';

class AddCommentScreen extends StatefulWidget {
  final Post post;

  AddCommentScreen(this.post);

  @override
  AddCommentScreenState createState() => AddCommentScreenState(post);
}

class AddCommentScreenState extends State<AddCommentScreen> {
  Post post;
  var _key = GlobalKey<ScaffoldState>();
  String commentError;
  var commentController = TextEditingController();

  AddCommentScreenState(this.post);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _key,
        appBar: AppBar(
          title: Text(Constants.ADD_COMMENT,
              style: TextStyle(fontWeight: FontWeight.w400)),
        ),
        body: GestureDetector(
          child: Container(
            margin: EdgeInsets.all(20),
            child: ListView(
              children: <Widget>[
                Text(
                  post.title,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.left,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                  child: TextField(
                    maxLines: null,
                    controller: commentController,
                    style: TextStyle(fontSize: 12, color: Colors.black87),
                    textInputAction: TextInputAction.newline,
                    decoration: InputDecoration(
                        hintText: Constants.COMMENT_HINT,
                        labelText: Constants.COMMENT,
                        errorText: commentError,
                        border: OutlineInputBorder(gapPadding: 1)),
                    keyboardType: TextInputType.text,
                    onChanged: (text) {
                      setState(() {
                        commentError = null;
                      });
                    },
                  ),
                ),
                Container(
                    margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                    child: RaisedButton(
                      color: Colors.blue,
                      textColor: Colors.white,
                      child: Text(Constants.COMMENT),
                      onPressed: () async {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            });
                        var flag = await _addComment();
                        Navigator.pop(context);
                        if (flag) {
                          _showSnackBar(Constants.COMMENT_SUCCESS);
                          Future.delayed(Duration(seconds: 2), () {
                            Navigator.pop(context);
                          });
                        }
                      },
                    )),
              ],
            ),
          ),
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
        ));
  }

  Future<bool> _addComment() async {
    var content = commentController.text;

    if (content.isEmpty) {
      setState(() {
        commentError = Constants.COMMENT_ERROR;
      });
      return false;
    }

    var status = await CommentService.instance.postComment(post.id, content);
    if (status.error == null) {
      return true;
    } else {
      _showSnackBar(status.error);
      return false;
    }
  }

  _showSnackBar(String message) {
    var snackBar = SnackBar(
      content: Text(message),
      duration: Duration(seconds: 2),
    );
    _key.currentState.showSnackBar(snackBar);
  }
}
