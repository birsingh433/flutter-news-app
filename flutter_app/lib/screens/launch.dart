import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:flutter_app/network/authentication_service.dart';
import 'package:flutter_app/screens/home.dart';
import 'package:flutter_app/utlis/constants.dart';

class LaunchScreen extends StatefulWidget{
  @override
  LaunchScreenState createState() => LaunchScreenState();
}


class LaunchScreenState extends State<LaunchScreen>{

  @override
  void initState() {
    super.initState();
    _getNonce();
  }

  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 4,
      navigateAfterSeconds: HomeScreen(),
      loadingText: Text(Constants.LOADING),
      title: Text(
          Constants.APP_NAME,
        style: TextStyle(
            fontSize: 40,
            fontWeight: FontWeight.normal,
            color: Colors.blue
        ),
      ),
    );
  }


  _getNonce()async{
    var nonce = await AuthenticationService.instance.getNonce();
    if (nonce.error == null){

    }
  }


}