
class CategoryList{
  List<Category> categories;

  CategoryList({
    this.categories
  });

  factory CategoryList.fromJson(List<dynamic> parsedJson){
    List<Category> categories = new List<Category>();
    categories = parsedJson.map((i)=>Category.fromJson(i)).toList();
    return new CategoryList(
      categories: categories,
    );
  }
}

class Category {
  final int id;
  final String name;

  Category(this.id, this.name);

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category(
      json['id'],
      json['name'],
    );
  }
}